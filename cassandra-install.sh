#!/bin/bash

echo "enter the os of machine you want to install cassandra"
read os


if [ $os == "debian" ]; then

echo "========================="
echo "check JAVA version and install JAVA"
echo "========================="

    sudo apt update 
    sudo apt install openjdk-8-jdk
    java –version

echo "========================="
echo "install the APT transport package. You need to add this package to your system to enable access to the repositories using HTTPS"
echo "========================="

    sudo apt install apt-transport-https -y

echo "========================="
echo "Adding the repo where the packages are available to the system"
echo "========================="

    echo "deb http://www.apache.org/dist/cassandra/debian 40x main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list
    echo "deb-src http://www.apache.org/dist/cassandra/debian 40x main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list

echo "========================="
echo "To avoid package signature warnings during package updates, we need to add the public key from the Apache Software Foundation associated with the package repositories"
echo "========================="
        
    gpg --keyserver pgp.mit.edu --recv-keys F758CE318D77295D
    gpg --export --armor F758CE318D77295D | sudo apt-key add -

echo "========================="
echo "Now install the cassandra"
echo "========================="    

    sudo apt install cassandra

echo "========================="
echo "Check cassandra version"
echo "========================="

    sudo cassandra -v

echo "========================="
echo "check the status of the cluster"
echo "========================="    

    sudo nodetool status

echo "========================="
echo "Cassandra installed checking status"
echo "========================="

    sudo systemctl status cassandra

echo "==========================================="

elif [ $os == "centos" ]; then

echo "========================="
echo "check JAVA version and install JAVA"
echo "========================="

      sudo yum update –y
      sudo yum install openjdk-8-jdk
      java –version

echo "========================="
echo "install the APT transport package. You need to add this package to your system to enable access to the repositories using HTTPS"
echo "========================="

      sudo yum install apt-transport-https

echo "========================="
echo "Adding the repo where the packages are available to the system"
echo "========================="

      echo "deb http://www.apache.org/dist/cassandra/debian 40x main" | sudo tee -a /etc/yum/sources.list.d/cassandra.sources.list
      echo "deb-src http://www.apache.org/dist/cassandra/debian 40x main" | sudo tee -a /etc/yum/sources.list.d/cassandra.sources.list

echo "========================="
echo "To avoid package signature warnings during package updates, we need to add the public key from the Apache Software Foundation associated with the package repositories"
echo "========================="
      
      gpg --keyserver pgp.mit.edu --recv-keys F758CE318D77295D
      gpg --export --armor F758CE318D77295D | sudo yum-key add -

echo "========================="
echo "Now install the cassandra"
echo "========================="

      sudo yum install cassandra

echo "========================="
echo "Check cassandra version"
echo "========================="

      sudo cassandra -v
      
echo "========================="
echo "check the status of the cluster"
echo "========================="

      sudo nodetool status

echo "========================="
echo "Cassandra installed checking status"
echo "========================="      

      sudo systemctl status cassandra

else
        echo "invalid input"

fi

